## Description

The Good Dogs Project recently published a blog post containing tips for traveling with your dog. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

You need to complete three tasks. You'll complete your work in the `/content/blog/travel-with-your-dog.md` file.

### Add a bulleted list

- [ ] Convert the information in the **Visiting the vet before you go** section to an unordered bulleted list. Use dashes (-) or asterisks (*) in front of each explanation.

### Change headings that use -ing 

- [ ] Change the headings that use -ing form to simple verb form.

### Fix typos

- [ ] Fix the typos in the first paragraph.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.